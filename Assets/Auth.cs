﻿using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Auth : MonoBehaviour {
    Scene scene;   
    public InputField username;
    public string userName = "Anon";

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    public void Update()
    {
        username = (InputField)FindObjectOfType(typeof(InputField));
        scene = SceneManager.GetActiveScene();
        if (scene.name=="Chatroom")
        {
            GameObject.FindGameObjectWithTag("username").GetComponent<Text>().text = userName;
        }
        
    }

    public void Authbtn()
    {
        
        Firebase.Auth.FirebaseAuth auth = Firebase.Auth.FirebaseAuth.DefaultInstance;

        auth.SignInAnonymouslyAsync().ContinueWith(task => {
            if (task.IsCanceled)
            {
                Debug.LogError("SignInAnonymouslyAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("SignInAnonymouslyAsync encountered an error: " + task.Exception);
                return;
            }

            Firebase.Auth.FirebaseUser newUser = task.Result;
            Debug.LogFormat("User signed in successfully: {0} ({1})",
                newUser.DisplayName, newUser.UserId);

            userName = username.text;
            SceneManager.LoadScene("Chatroom");
        });
    }
}
