﻿using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Chat : MonoBehaviour {

    bool dirtyflag= false;
    bool firstTime = true;
    bool loadflag = false;

    public GameObject usertext;
    public InputField messageTxt;
    public string text;
    

    public GameObject msjBoard;
    public GameObject canvas;
    public GameObject messages;

    public ScrollRect scrollRect;
    public Transform middle;

    ArrayList ChatListusername = new ArrayList();
    ArrayList ChatListmsg = new ArrayList();
    ArrayList stamptime = new ArrayList();

    int count = 0;
    int limit = 6;
    int newMsg = -1;
   
    float yukari = -200f;

    DependencyStatus dependencyStatus = DependencyStatus.UnavailableOther;

    void Start () {

        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
            dependencyStatus = task.Result;
            if (dependencyStatus == DependencyStatus.Available)
            {
                FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://friends-849ef.firebaseio.com/");

                DatabaseReference reference = FirebaseDatabase.DefaultInstance.RootReference;

                Listener();
                
                
            }
            else
            {
                Debug.LogError(
                  "Could not resolve all Firebase dependencies: " + dependencyStatus);
            }
        });

    }

    private void LateUpdate()
    {
        messageTxt = (InputField)FindObjectOfType(typeof(InputField));
        text = messageTxt.text;

        if (dirtyflag)
        {
            Debug.Log("dirtflag içinde");
            if (firstTime)
            {
                Debug.Log("ilk defa");
                for (int i = 0; i < count; i++)
                {
                    var board = Instantiate(msjBoard) as GameObject;
                    board.transform.SetParent(messages.transform);
                    board.name = i.ToString();
                    board.transform.position = new Vector3(canvas.transform.position.x, canvas.transform.position.y + yukari, 0);
                    yukari += 150f;

                    board.gameObject.transform.GetChild(1).GetComponent<Text>().text = ChatListmsg[i].ToString(); //mesaj
                    board.gameObject.transform.GetChild(2).GetComponent<Text>().text = ChatListusername[i].ToString(); //username       
                }
                firstTime = false;
            }
            else
            {
                if (loadflag)
                {
                    for (int i = 0; i < count-1; i++)
                    {
                        
                        var board = Instantiate(msjBoard) as GameObject;
                        board.transform.SetParent(messages.transform);
                        board.name = (i+limit).ToString();
                        board.transform.position = new Vector3(canvas.transform.position.x, GameObject.Find((i+limit-1).ToString()).gameObject.transform.position.y + 150, 0);

                        board.gameObject.transform.GetChild(1).GetComponent<Text>().text = ChatListmsg[i+1].ToString(); //mesaj
                        board.gameObject.transform.GetChild(2).GetComponent<Text>().text = ChatListusername[i+1].ToString(); //username       
                    }
                    limit += 5;
                    loadflag = false;
                }
                else
                {
                    //for (int i = count - 1; i >= 0; i--)
                    //{
                    //    if (i >= 1)
                    //    {
                    //        var board = GameObject.Find((i - 1).ToString());
                    //        GameObject.Find((i).ToString()).gameObject.transform.GetChild(1).GetComponent<Text>().text = board.gameObject.transform.GetChild(1).GetComponent<Text>().text;
                    //        GameObject.Find((i).ToString()).gameObject.transform.GetChild(2).GetComponent<Text>().text = board.gameObject.transform.GetChild(2).GetComponent<Text>().text;

                    //    }
                    //    else
                    //    {
                    //        var board = GameObject.Find((i).ToString());
                    //        board.gameObject.transform.GetChild(1).GetComponent<Text>().text = ChatListmsg[i].ToString(); //mesaj
                    //        board.gameObject.transform.GetChild(2).GetComponent<Text>().text = ChatListusername[i].ToString(); //username 
                    //    }

                    //} 

                    var board = Instantiate(msjBoard) as GameObject;
                    board.transform.SetParent(messages.transform);
                    board.name = (newMsg).ToString();
                    board.transform.position = new Vector3(canvas.transform.position.x, GameObject.Find((newMsg+1).ToString()).gameObject.transform.position.y - 150, 0);
                    newMsg--;

                    board.gameObject.transform.GetChild(1).GetComponent<Text>().text = ChatListmsg[0].ToString(); //mesaj
                    board.gameObject.transform.GetChild(2).GetComponent<Text>().text = ChatListusername[0].ToString(); //username 

                    messages.transform.position = new Vector3(messages.transform.position.x, messages.transform.position.y + 150f, messages.transform.position.z);

                }
                
            }
            dirtyflag = false;
            
        }
        

    }

    public class Message
    {
        public string username;
        public string messagetxt;
        public int timestamp;

        public Message() { }

        public Message(string username, string messagetxt, int timestamp)
        {
            this.username = username;
            this.messagetxt = messagetxt;
            this.timestamp = timestamp;
        }

    }

    public void setMessage(string username, string messagetxt, int timestamp)
    {
        DatabaseReference reference = FirebaseDatabase.DefaultInstance.RootReference;

        Message message = new Message(username, messagetxt,timestamp);
        string json = JsonUtility.ToJson(message);

        reference.Child("chatroom").Child(timestamp.ToString()).SetRawJsonValueAsync(json);
    }

    public void SendMessage()
    {
        setMessage(usertext.GetComponent<Text>().text, text,(int)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds);     
    }

    protected void Listener()
    {
        FirebaseDatabase.DefaultInstance
        .GetReference("chatroom").OrderByChild("timestamp").LimitToLast(6)
        .ValueChanged += HandleValueChanged;

    }

    public void MsgGetir()
    {
        FirebaseDatabase.DefaultInstance
       .GetReference("chatroom").OrderByKey().EndAt(stamptime[count-1].ToString()).LimitToLast(6)
       .ValueChanged += HandleValueChanged;

        loadflag = true;

    }


    void HandleValueChanged(object sender, ValueChangedEventArgs args)
    {
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }
        // Do something with the data in args.Snapshot
        ChatListusername.Clear();
        ChatListmsg.Clear();
        stamptime.Clear();
        count = 0;
        foreach (var childSnapshot in args.Snapshot.Children)
        {
            Debug.Log("Girilen Mesajlar : " +
                        childSnapshot.Child("username").Value.ToString() + " - " +
                        childSnapshot.Child("messagetxt").Value.ToString());
            Debug.Log("HandleValueChange çalıştı");
            ChatListusername.Insert(0, childSnapshot.Child("username").Value.ToString());
            ChatListmsg.Insert(0, childSnapshot.Child("messagetxt").Value.ToString());
            stamptime.Insert(0, childSnapshot.Child("timestamp").Value.ToString());
            count++;
            
        }
        Debug.Log(count);
        dirtyflag = true;
    }



    void OnEnable()
    {
        //Subscribe to the ScrollRect event
        scrollRect.onValueChanged.AddListener(scrollRectCallBack);
    }

    //Will be called when ScrollRect changes
    void scrollRectCallBack(Vector2 value)
    {
       // Debug.Log("ScrollRect Changed: " + value);
        if (value.magnitude > 1f)// UP
        {
          //  Debug.Log("yukarı gösteriyor");
            if (!loadflag)
            {
                //if (GameObject.Find((limit - 1).ToString()).transform.position.y < middle.transform.position.y)
                //{
                //    MsgGetir();
                //}
            }
        }
        else if(value.magnitude < 0.5f)
        {
         //   Debug.Log("aşağı gösteriyor");
        }
    }

    void OnDisable()
    {
        //Un-Subscribe To ScrollRect Event
        scrollRect.onValueChanged.RemoveListener(scrollRectCallBack);
    }

    //public void LoadNewMessages()
    //{
    //    FirebaseDatabase.DefaultInstance
    //  .GetReference("chatroom").OrderByValue()
    //  .GetValueAsync().ContinueWith(task =>
    //  {
    //      if (task.IsFaulted)
    //      {
    //          // Handle the error...
    //          Debug.Log("veriler getirilemedi");
    //      }
    //      else if (task.IsCompleted)
    //      {
    //          DataSnapshot snapshot = task.Result;
    //          ChatListusername.Clear();
    //          ChatListmsg.Clear();
    //          stamptime.Clear();

    //          foreach (var childSnapshot in snapshot.Children)
    //          {
    //              //Debug.Log("Girilen Mesajlar : " +
    //              //            childSnapshot.Child("username").Value.ToString() + " - " +
    //              //            childSnapshot.Child("messagetxt").Value.ToString() + " - " +
    //              //            childSnapshot.Child("timestamp").Value.ToString());
    //              Debug.Log("database okundu-----veriler çekildi-----------");
    //              ChatListusername.Insert(0, childSnapshot.Child("username").Value.ToString());
    //              ChatListmsg.Insert(0, childSnapshot.Child("messagetxt").Value.ToString());
    //              stamptime.Insert(0, childSnapshot.Child("timestamp").Value.ToString());
    //              count++;


    //          }

    //      }

    //  });

    //}

    //public void ShowMessages()
    //{
    //    for (int i =0; i < limit; i++)
    //    {
    //        var board = Instantiate(msjBoard) as GameObject;
    //        board.transform.SetParent(messages.transform);
    //        board.name = (i+limit).ToString();
    //        board.transform.position = new Vector3(canvas.transform.position.x, canvas.transform.position.y + yukari, 0);
    //        yukari += 150f;

    //        board.gameObject.transform.GetChild(1).GetComponent<Text>().text = ChatListmsg[i+limit].ToString(); //mesaj
    //        board.gameObject.transform.GetChild(2).GetComponent<Text>().text = ChatListusername[i+limit].ToString(); //username       
    //    }
    //}

}


